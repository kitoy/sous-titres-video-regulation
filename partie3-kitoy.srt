﻿1
00:00:08,820 --> 00:00:11,320
 Alors ça, c'est un truc qui est simple à comprendre.

2
00:00:11,520 --> 00:00:17,280
Le régulateur fixe quelques règles du jeu qui s'appliquent

3
00:00:17,440 --> 00:00:27,820
à des organismes de bienfaisance, des PME que sont: Orange, SFR, Free, Bouygues, Vinci.

4
00:00:28,000 --> 00:00:31,200
L'ARCEP a obligation de consulter les acteurs économiques.

5
00:00:31,220 --> 00:00:37,228
Lorsque l''ARCEP fait ça elle va consulter des acteurs experts, mais sur une branche très réduite de marché

6
00:00:37,820 --> 00:00:41,142
Donc elle va interroger un opérateur

7
00:00:41,170 --> 00:00:44,114
spécialisé dans Internet pour les avocats,

8
00:00:44,257 --> 00:00:49,170
qui va leur expliquer comment son business va mourir si sur cette frange là du marché

9
00:00:49,200 --> 00:00:53,942
on touche a tel ou tel paramètre. Donc c'est des gens qui sont extrêmement expert mais qui ont

10
00:00:54,000 --> 00:00:57,820
un angle de vue qui est minuscule, ils ne regardent que leur segment du marché.

11
00:00:58,000 --> 00:01:01,571
En grande parti parce que ce qui les intéresses, c'est de sauver leur business.

12
00:01:01,571 --> 00:01:06,820
On sur un marché économique capitaliste rangé correctement; les gens veulent gagner du pognon,

13
00:01:07,000 --> 00:01:10,742
C'est... comme ça. Et nous on arrive sur ce marché,

14
00:01:10,800 --> 00:01:16,257
En étant opérateur en participant au jeu, en jouant comme les autres mais avec une vision du monde,

15
00:01:16,420 --> 00:01:20,171
qui n'est pas "il faut gagner du pognon", on s'en fiche vraiment de gagner du pognon.

16
00:01:20,200 --> 00:01:25,257
Si l'abonnement on doit le vendre à 10€ à 50€ à 100€,

17
00:01:25,520 --> 00:01:31,520
on le vendra à 10, 20, 50 euros. A 50 on aura plus de mal d'avoir des abonnés qu'a 10 forcément,

18
00:01:31,820 --> 00:01:36,000
mais ce n'est pas tellement le problème, on est des associations de bénévoles

19
00:01:36,000 --> 00:01:38,371
notre but premier c'est pas de gagner de l'argent

20
00:01:38,371 --> 00:01:41,250
pour pouvoir grossir et nourrir des actionnaires

21
00:01:41,257 --> 00:01:45,320
ce n'est pas le sujet, le sujet de nos associations, c'est de défendre la neutralité du net

22
00:01:45,320 --> 00:01:48,400
et de promouvoir la décentralisation des réseaux. Ce n'est pas pareil.

23
00:01:48,520 --> 00:01:54,342
Du coup, ça fait arriver sur le marché et dans la discussion avec le régulateur des acteurs

24
00:01:54,510 --> 00:01:56,771
qui n'ont pas une vue étroite mais une vue large.

25
00:01:57,000 --> 00:01:59,571
Ça fait arriver dans le bureau du régulateur

26
00:01:59,620 --> 00:02:01,942
des gens comme moi qui on fait des études de philosophie,

27
00:02:02,000 --> 00:02:05,310
et qui viennent leur expliquer la philosophie. pour parler de télécoms.

28
00:02:05,457 --> 00:02:10,000
Et avec cet angle de vue là, avec cette manière de cabler les choses de manière très inattendu,

29
00:02:10,140 --> 00:02:12,257
on arrive a un angle d'analyse qui est plus large,

30
00:02:12,285 --> 00:02:15,280
et on arrive à toucher des problèmes de société

31
00:02:15,280 --> 00:02:18,000
que l'ARCEP voit pas parce qu'elle n'a pas les bons outils.

32
00:02:18,000 --> 00:02:23,520
Quand les acteurs économique se font la guerre entre eux, quels dommages collatéraux il y a pour les gens ?

33
00:02:23,600 --> 00:02:25,980
Quels dommages collatéraux il y a sur la société ? 

34
00:02:26,180 --> 00:02:28,320
En quoi est-ce qui ça déforme la société ?

35
00:02:28,320 --> 00:02:31,000
Google qui se bastonne contre Facebook 

36
00:02:31,200 --> 00:02:35,320
pour savoir qui aura le plus de parts de marché dans le marché publicitaire.

37
00:02:35,320 --> 00:02:36,942
C'est une question économique,

38
00:02:36,971 --> 00:02:40,742
et cela a des conséquences colossales sur la vie privée des gens,

39
00:02:40,840 --> 00:02:43,020
sur la façon dont la société se structure

40
00:02:43,320 --> 00:02:47,520
sur la façon dont les haines montent ou ne montent pas à l’intérieur des sociétés.

41
00:02:47,820 --> 00:02:50,520
C'est des conséquences extrêmement lourdes.

42
00:02:50,820 --> 00:02:54,971
On a une place très précise sur ce marché là, nous sommes les seuls à avoir les deux pieds,

43
00:02:55,020 --> 00:02:58,600
On a un pied dans le marché, parce qu'on est opérateur on a cette activité-là.

44
00:02:58,650 --> 00:03:02,000
On est extrêmement expert sur la question de qu'est-ce qu'un opérateur ?

45
00:03:02,057 --> 00:03:04,970
On a des opérateurs qui datent d'avant qu'Orange arrive.

46
00:03:05,000 --> 00:03:07,314
On a FDN c'est 1992.

47
00:03:07,340 --> 00:03:09,714
et on a un pied complètement en dehors

48
00:03:09,740 --> 00:03:12,000
car on est des gens de la "société civile".

49
00:03:12,050 --> 00:03:17,000
On est bénévoles. On a tous un autre métier à coté du fait d'être opérateur,

50
00:03:17,310 --> 00:03:20,371
et pour certains l'autre métier n'a rien à voir; cela produit

51
00:03:20,450 --> 00:03:22,685
une ouverture d'esprit une largesse de vue

52
00:03:22,770 --> 00:03:24,828
qui est très intéressante et importante

53
00:03:24,885 --> 00:03:27,850
qui permet d'aider le régulateur à assumer

54
00:03:28,520 --> 00:03:32,320
cette mission de défense de la neutralité du net pour laquelle elle n'est pas armé.

55
00:03:32,400 --> 00:03:34,640
On est un peu le bras armé du régulateur, mais pas rétribuer

56
00:03:34,680 --> 00:03:38,440
en parti parce qu'on a cette vision d’intérêt générale que même l'ARCEP n'a pas.

57
00:03:38,514 --> 00:03:40,420
L'ARCEP n'a pas de vision d'intérêt général.

58
00:03:40,520 --> 00:03:43,771
Nous si, car on part d'abord de l'utilisateur final pour arrivé sur le marché,

59
00:03:43,820 --> 00:03:47,342
alors que l'ARCEP part du marché pour arrivé peut-être à l'utilisateur final.

60
00:03:47,342 --> 00:03:51,200
On prouve souvent qu'il y a un mouvement qui marche mieux que l'autre.


